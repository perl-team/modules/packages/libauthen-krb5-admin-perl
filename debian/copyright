Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Authen-Krb5-Admin
Upstream-Contact: Stephen Quinney <squinney@inf.ed.ac.uk>
Source: https://metacpan.org/release/Authen-Krb5-Admin

Files: *
Copyright: © 2002, Andrew J. Korty
License: BSD-2-clause

Files: admin.h
Copyright: © 2001, Massachusetts Institute of Technology
License: MIT-Kerberos
 Export of this software from the United States of America may
 require a specific license from the United States Government.
 It is the responsibility of any person or organization contemplating
 export to obtain such a license before exporting.
 .
 WITHIN THAT CONSTRAINT, permission to use, copy, modify, and
 distribute this software and its documentation for any purpose and
 without fee is hereby granted, provided that the above copyright
 notice appear in all copies and that both that copyright notice and
 this permission notice appear in supporting documentation, and that
 the name of M.I.T. not be used in advertising or publicity pertaining
 to distribution of the software without specific, written prior
 permission.  Furthermore if you modify this software you must label
 your software as modified software and not distribute it in such a
 fashion that it might be confused with the original M.I.T. software.
 M.I.T. makes no representations about the suitability of
 this software for any purpose.  It is provided "as is" without express
 or implied warranty.

Files: ppport.h
Copyright: 2004-2009, Marcus Holland-Moritz <mhx-cpan@gmx.net>
 2001, Paul Marquess <pmqs@cpan.org> (Version 2.x)
 1999, Kenneth Albanowski <kjahds@kjahds.com> (Version 1.x)
License: Artistic or GPL-1+
 This program is free software; you can redistribute it and/or
 modify it under the same terms as Perl itself.
 .
 Perl is distributed under your choice of the GNU General Public License
 version 1 or later, or the Artistic License.  On Debian GNU/Linux
 systems, the complete text of the GNU General Public License can be found
 in `/usr/share/common-licenses/GPL-1' and the Artistic Licence in
 `/usr/share/common-licenses/Artistic'.

Files: debian/*
Copyright:
 © 2008, 2010, Ansgar Burchardt <ansgar@debian.org>
 © 2009-2022, gregor herrmann <gregoa@debian.org>
 © 2011, Russ Allbery <rra@debian.org>
License: BSD-2-clause

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
